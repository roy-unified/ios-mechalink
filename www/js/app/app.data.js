var app = angular.module('mechanlink.app.data', []);
app.config(function($httpProvider) {
      //Enable cross domain calls
      $httpProvider.defaults.useXDomain = true;

      //Remove the header used to identify ajax call  that would prevent CORS from working
      delete $httpProvider.defaults.headers.common['X-Requested-With'];
  });
app.factory("Data", ['$http', 'toaster',function ($http, toaster){

var serviceBase = 'http://mechanlink.co.uk/api/v1.1/';




var obj = {};
var config = {headers:  {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'Auth': ''+localStorage.getItem('userAuthToken')
    }
};




obj.toast = function (type,title,message)
{

  toaster.pop(type, title , message, 10000, 'trustedHtml');
}

obj.get = function (q) {

           return $http.get(serviceBase + q, config).then(function (results) {
               return results.data;
           }, function (results) {
               console.log("fetching error from http");
               console.log(results.error);
               return null;
           });

       };
obj.post = function (q, object) {

           return $http.post(serviceBase + q, object, config).then(function (results) {
               return results.data;
           });
       };
obj.put = function (q, object) {
           return $http.put(serviceBase + q, object, config).then(function (results) {
               return results.data;
           });
       };
obj.delete = function (q) {
           return $http.delete(serviceBase + q,config).then(function (results) {
               return results.data;
           });
       };

//refresh state
obj.refresh = function()
{
  console.log("running dataRefreshed ");


    window.location.reload(true)

}

//Check if userAuthToken is valid else goto login page
obj.checkAuthToken = function () {
  //window.location.reload(true);
  //check if the token exits
  if(localStorage.getItem('userAuthToken') == null || localStorage.getItem('userAuthToken') == 'undefined' || !localStorage.getItem('userAuthToken') )
  {
    console.log("no apikey set");
    //redirect user to login screen to login
    return null;
  }
  else{
var authtoken = localStorage.getItem('userAuthToken');
console.log(authtoken);
  }
  var q = "checkauthtoken/";
           return $http.get(serviceBase + q + authtoken).then(function (results) {
               return results.data;


           });

       };


       return obj;
}]);
