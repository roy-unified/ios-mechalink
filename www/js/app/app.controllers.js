angular.module('mechanlink.app.controllers', [])

.controller('ProfileCtrl', function($scope, $ionicHistory, $state, $ionicScrollDelegate, loggedUser, user, followers, following, posts, pictures, services, isTrusted, Data) {
  $scope.$on('$ionicView.afterEnter', function() {
    $ionicScrollDelegate.$getByHandle('profile-scroll').resize();
  });

  $scope.currentUserId = user.id;
  $scope.user = user;
  $scope.loggedUser = loggedUser;
  $scope.currentUserIsTrusted = isTrusted;
$scope.wonbids = "";
  //is this the profile of the logged user?
  $scope.myProfile = loggedUser.id == user.id;

  $scope.user.followers = followers;
  $scope.user.following = following;
  $scope.user.posts = posts;
  $scope.user.services = services;
  $scope.user.pictures = pictures;
  $scope.accountType = user.accounttype == "mechanic";
$scope.wonbids ="";

  Data.get('notification/new').then(function(results)
{

  console.log("user new notification count from ProfileCtrl");
  console.log(results);
  $scope.newnotificationscount = results;
});

Data.get('notification/all').then(function(results)
{

console.log("user notification result");
console.log(results);
$scope.notifications = results;
$scope.lt = results.length;

});

//get all user won bids
  Data.get('allwonbids/').then(function(results)
{
    console.log("User won bids");
$scope.wonbids = results.wonbids;

  console.log($scope.wonbids);

})



  $scope.getUserPosts = function(){

    $ionicHistory.currentView($ionicHistory.backView());
    $ionicHistory.nextViewOptions({ disableAnimate: true });
    $state.go('app.profile.posts', {userId: user.id});
  };


  $scope.getUserServices = function(){

    $ionicHistory.currentView($ionicHistory.backView());
    $ionicHistory.nextViewOptions({ disableAnimate: true });
    $state.go('app.profile.services', {userId: user.id});
  };
})



.controller('sideMenuCtrl', function($scope, $ionicHistory, $state) {
  //we need to do this in order to prevent the back to change
  $ionicHistory.currentView($ionicHistory.backView());
  $ionicHistory.nextViewOptions({ disableAnimate: true });
$scope.loggedUserId = localStorage.getItem("userId");
$scope.loggedUserIsMechanic = localStorage.getItem("ismechanic");

})

.controller('customNavCtrl', function($scope, $state, $ionicHistory) {
$scope.gotoNotifications = function()
{
  //$ionicHistory.currentView($ionicHistory.backView());
  //$ionicHistory.nextViewOptions({ disableAnimate: true });
  $state.go("app.profile.notifications",{userId: $scope.currentUserId});
}


})

.controller('CommentsCtrl', function($scope, $state, $ionicPopup, FeedService, Data, $rootScope,GooglePlacesService) {
  var commentsPopup = {};
  var postid = "";
  var postdata =""
  $scope.newcomment = "";
  $scope.hasComment = "";

  Data.get('userinfo').then(function(results)
  {
    $scope.loggedUserData = results;
    console.log(results);

  })

  $scope.showComments = function(post) {
    console.log("showing commentss");


      GooglePlacesService.callAnalyticsEvent("Click","Showing comments","Showing comments");

    postdata = post;
    FeedService.getPostComments(post)
    .then(function(data){
      postid = post.id;
      console.log(data);
      post.comments_list = data;
      var commentphrase = ""
      if(post.comments == 0 || post.comments == 1){commentphrase = "Comment";}else{commentphrase = "Comments"};
      $scope.commentscount = post.comments;
      commentsPopup = $ionicPopup.show({
        cssClass: 'popup-outer comments-view',
        templateUrl: 'views/app/partials/comments.html',
        scope: angular.extend($scope, {current_post: post}),
        title: post.comments + ' '+commentphrase,
        buttons: [
          { text: '', type: 'close-popup ion-ios-close-outline' }
        ]
      });
    });
  };

  //Post a new comment
  $scope.postComment = function(post)
  {
    $scope.showtest = false;
    console.log("post owner id");
console.log(post.user.id);
    console.log("postid: "+post.id);
    var encodedCred = 'comment=' + encodeURIComponent($scope.newcomment);
    Data.post('comment/'+post.id,encodedCred).then(function(results)
  {
    //if comment was posted, create notification
    if(!results.error)
    {
      $scope.createNotification(post.user.id, localStorage.getItem("userId"), post.title, 2, post.id);
    }
console.log("fetching.. fresh");
    FeedService.getPostComments($scope.post)
    .then(function(data){
      console.log("fetching new comment data");
      console.log(data);
      $scope.post.comments_list = data;
      $scope.hasComment = true;
      $scope.newcomment = "";
    });
  });
  };

  //delete a comment
  $scope.deleteComment = function(comment)
  {

    Data.delete('comment/'+comment.id).then(function(results)
  {
console.log("results from deleting comment");
console.log(results.message);
if(!results.error){$scope.post.comments_list.splice($scope.post.comments_list.indexOf(comment), 1)};

    });
  }

  //Create a new notification for action
  //notification_type :the type of notification, i.e: bid -1, comment -2, review -3, trusted -4, rate -5,
  $scope.createNotification = function(for_user_id, trigger_user_id, message, notification_type, reference_post_id)
  {
    var encodedCred = 'for_user_id=' + encodeURIComponent(for_user_id)
    +'&trigger_user_id=' + encodeURIComponent(trigger_user_id)
    +'&message=' + encodeURIComponent(message)
    +'&notification_type=' + encodeURIComponent(notification_type)
    +'&reference_id=' + encodeURIComponent(reference_post_id);


    Data.post('notification',encodedCred).then(function(results)
  {
console.log("results from creating notification");
console.log(results.message);

    });
  }




  //CLICK IN USER NAME
  $scope.navigateToUserProfile = function(user){

      GooglePlacesService.callAnalyticsEvent("Click","Navigate to user profile from comments","Navigate to user profile from comments");

      commentsPopup.close();
    if(user.ismechanic){
    $state.go('app.profile.services', {userId: user.id});
  }else {
    {
      $state.go('app.profile.posts', {userId: user.id});
    }
  }
  };


})



.controller('BiddsCtrl', function($scope, $state, $ionicPopup, FeedService, Data, $ionicLoading,GooglePlacesService) {

  var biddsPopup = {};
  $scope.bidamount = "";
  $scope.bidproposal = "";
  $scope.showacceptedbid = false;
  Data.get('userinfo').then(function(results)
  {
    $scope.loggedUserData = results;

  });

$scope.loggedUserIsMechanic = localStorage.getItem("ismechanic");



  $scope.showBidds = function(post) {

    GooglePlacesService.callAnalyticsEvent("Click","Showing Bids","Showing Bids");

    FeedService.getPostBidds(post)
    .then(function(data){
      console.log("post bids");
      console.log(data);
      $scope.getAcceptedBid(post.id);
      post.bidds_list = data.bid_list;

      var bidphrase = "";
      if(post.bidds == 1 || post.bidds == 0){bidphrase = "Bid"}else{bidphrase = "Bids"};
      $scope.biddsPopup = $ionicPopup.show({
        cssClass: 'popup-outer comments-view',
        templateUrl: 'views/app/partials/bidds.html',
        scope: angular.extend($scope, {current_post: post}),
        title: post.bidds+' '+bidphrase,
        buttons: [
          { text: '', type: 'close-popup ion-ios-close-outline' }
        ]
      });
    });
  };

  $scope.getAcceptedBid = function(job_id)
  {
    console.log("Bid to remove");

    Data.get('acceptedbid/'+job_id).then(function(results)
  {
    console.log("Accepted bid");
    console.log(job_id);
    if(results == "Null")
    {

      $scope.acceptedbid = "null";
      $scope.showacceptedbid = false;
      console.log($scope.acceptedbid);
    }else {
      $scope.showacceptedbid = true;
      $scope.acceptedbid = results;
      console.log($scope.acceptedbid);
if($scope.current_post.bidds_list != "undefined"){

$scope.current_post.bidds_list = $scope.current_post.bidds_list.splice($scope.current_post.bidds_list.indexOf($scope.acceptedbid.bid), 1);

}

    }
  });
  }

  //Posting a new bid for a job
  $scope.postBid = function(post)
  {
      GooglePlacesService.callAnalyticsEvent("Click","Posting Bid","Posting Bid");

    console.log(post.id);
    console.log("bidproposal: "+$scope.bidproposal+" amount: "+$scope.bidamount);
    var encodedCred = 'proposal=' + encodeURIComponent($scope.bidproposal)+'&amount=' + encodeURIComponent($scope.bidamount);

    if($scope.bidproposal == "null" || $scope.bidamount)
    Data.post('user/bid/'+post.id,encodedCred).then(function(results)
  {
    //if bid was posted, create notification
    if(!results.error)
    {

      $scope.current_post.bidded = true;
      post.bidded = true;
      $scope.createNotification(post.user.id, localStorage.getItem("userId"), post.title, 1, post.id);
    }
    console.log(results);
console.log("fetching.. fresh bids");
    FeedService.getPostBidds($scope.post)
    .then(function(data){
      console.log("fetching new bid data");
      console.log(data);
      $scope.post.bidds_list = data.bid_list;
      $scope.post.bidds = "200";
      $scope.bidproposal = "";
      $scope.bidamount = "";
    });
  });
  };



  //Accept bid
  $scope.acceptBid = function(bid,post)
  {

 GooglePlacesService.callAnalyticsEvent("Click","Accept Bids","Accept Bids");


      console.log(bid.id);
    console.log("bid_id: "+bid.id+" job_id: "+bid.job_id +" won_user_id: "+bid.user.id);
    var encodedCred = 'job_id=' + encodeURIComponent(bid.job_id)+'&won_user_id=' + encodeURIComponent(bid.user.id);
    var confirmPopup = $ionicPopup.confirm({
      title: 'Accept Bid',
      template: 'Are you sure you want to accept the bid for <strong>"'+'£'+bid.amount+'"</strong>?'
    });
    confirmPopup.then(function(res) {
      if(res) {
        console.log('Yes accepted bid confirmed');
        console.log("Accepting bid:"+bid.id);

    Data.post('acceptbid/'+bid.id,encodedCred).then(function(results)
  {
    if(!results.error)
    {
      $scope.showacceptedbid = true;
      //remove accepted bid from the bid list
      $scope.post.bidds_list.splice($scope.post.bidds_list.indexOf(bid), 1);

      //assign accepted bid
      $scope.acceptedbid = bid;
      Data.toast("success","Bid Accepted",results.message);
      console.log(results);
//create notification for accepted bid
      $scope.createNotification(bid.user.id, localStorage.getItem("userId"), "Congratulations! You won the Job : "+post.title+". Now contact the job owner, and get it cracking.", 5, bid.job_id);
    }else {
        Data.toast("error","Bid Accepted",results.message);
    }

  });
} else {
  console.log("No don't accepted bid confirmed");
}
});
  };

  //delete a bid
  $scope.rejectAcceptedBid = function(acceptedbid,post)
  {
      GooglePlacesService.callAnalyticsEvent("Click","Reject accepted bid","Reject accepted bid");

      var confirmPopup = $ionicPopup.confirm({
      title: 'Reject Accepted Bid',
      template: 'Are you sure you want to reject the bid for <strong>"'+'£'+acceptedbid.bid.amount+'"</strong>?'
    });
    confirmPopup.then(function(res) {
      if(res) {
        console.log('Yes acceptedbid confirmed');
        console.log("Rejecting acceptedbid:"+acceptedbid.bid.id);

        Data.delete('/acceptedbid/'+acceptedbid.bid.job_id).then(function(results)
      {
      console.log("results from deleting bid");
      console.log(results.message);
      console.log(acceptedbid.bidder.photo);
      if(!results.error){
        $scope.acceptedbid = "Null";
        $scope.showacceptedbid = false;
        $scope.createNotification(acceptedbid.bidder.id, localStorage.getItem("userId"), "Oops! Your accepted bid for job : "+post.title+", was rejected by the job owner. Don't worry, you still have time to submit another bid.", 6, acceptedbid.bid.job_id);

    $scope.putbackbid =
    {
    created_at : acceptedbid.bid.created_at,
      amount: acceptedbid.bid.amount,
      user: acceptedbid.bidder,
      proposal: acceptedbid.bid.proposal,
    };
    console.log($scope.putbackbid);
        $scope.post.bidds_list.push($scope.putbackbid);
        Data.toast("success","Accepted Bid", "You have rejected the currently accepted bid")
      };

        });
      } else {
        console.log("No don't delete acceptedbid confirmed");
      }
    });


  }

  //delete a bid
  $scope.deleteBidd = function(bidd)
  {

      GooglePlacesService.callAnalyticsEvent("Click","Delete bid","Delete bid");


      Data.delete('jobs/bid/'+bidd.id).then(function(results)
  {
  console.log("results from deleting bid");
  console.log(results.message);
  if(!results.error){
    $scope.current_post.bidded = false;
    $scope.post.bidds_list.splice($scope.post.bidds_list.indexOf(bidd), 1)};

    });
  }


  //Create a new notification for action
  //notification_type :the type of notification, i.e: bid -1, comment -2, review -3, trusted -4,
  $scope.createNotification = function(for_user_id, trigger_user_id, message, notification_type, reference_post_id)
  {
    var encodedCred = 'for_user_id=' + encodeURIComponent(for_user_id)
    +'&trigger_user_id=' + encodeURIComponent(trigger_user_id)
    +'&message=' + encodeURIComponent(message)
    +'&notification_type=' + encodeURIComponent(notification_type)
    +'&reference_id=' + encodeURIComponent(reference_post_id);

    Data.post('notification',encodedCred).then(function(results)
  {
console.log("results from creating notification");
console.log(results.message);

    });
  }


  //CLICK IN USER NAME
  $scope.navigateToUserProfile = function(user){

 GooglePlacesService.callAnalyticsEvent("Click","Navigate to user profile from bids","Navigate to user profile from bids");

      $scope.biddsPopup.close();
    if(user.ismechanic){
    $state.go('app.profile.services', {userId: user.id});
  }else {
    {
      $state.go('app.profile.posts', {userId: user.id});
    }
  }
  };
})


.controller('ReviewsCtrl', function($scope, $state, $ionicPopup, FeedService, Data,GooglePlacesService) {
  var reviewsPopup = {};

  Data.get('userinfo').then(function(results)
  {
    $scope.loggedUserData = results;
    console.log(results);

  });

  $scope.newreview = "";

  $scope.showReviews = function(post) {
    FeedService.getPostReviews(post)
    .then(function(data){
      console.log("Service reviews");
      console.log(data);
      post.reviews_list = data;
      reviewsPopup = $ionicPopup.show({
        cssClass: 'popup-outer comments-view',
        templateUrl: 'views/app/partials/reviews.html',
        scope: angular.extend($scope, {current_post: post}),
        title: post.reviews+' Reviews',
        buttons: [
          { text: '', type: 'close-popup ion-ios-close-outline' }
        ]
      });
    });
  };

  //Post a postive new review
  $scope.postPostiveReview = function(post)
  {
    var encodedCred = 'review=' + encodeURIComponent($scope.newreview)
                        +'&positive=' + encodeURIComponent("1");
                        console.log("review data to post");
                        console.log(encodedCred);
    Data.post('review/'+post.id,encodedCred).then(function(results)
  {
    //if review was posted, create notification
    if(!results.error)
    {
      $scope.createNotification(post.user.id, localStorage.getItem("userId"), post.title, 3, post.id);
    }
    console.log(results);
console.log("fetching.. fresh reviews");
    FeedService.getPostReviews($scope.service)
    .then(function(data){
      console.log("fetching new review data");
      console.log(data);
      post.reviews_list = data;
      $scope.newreview = "";
    });
  });
  };


  //Post a new review
  $scope.postNegativeReview = function(post)
  {
    var encodedCred = 'review=' + encodeURIComponent($scope.newreview)
                        +'&positive=' + encodeURIComponent("0");
    Data.post('review/'+post.id,encodedCred).then(function(results)
  {
    //if review was posted, create notification
    if(!results.error)
    {
      $scope.createNotification(post.user.id, localStorage.getItem("userId"), post.title, 3, post.id);
    }
console.log("fetching.. fresh reviews");
    FeedService.getPostReviews($scope.service)
    .then(function(data){
      console.log("fetching new review data");
      console.log(data);
      post.reviews_list = data;
      $scope.hasReview = true;
      $scope.newreview = "";
    });
  });
  };


  //Create a new notification for action
  //notification_type :the type of notification, i.e: bid -1, comment -2, review -3, trusted -4, rate -5,
  $scope.createNotification = function(for_user_id, trigger_user_id, message, notification_type, reference_post_id)
  {
    var encodedCred = 'for_user_id=' + encodeURIComponent(for_user_id)
    +'&trigger_user_id=' + encodeURIComponent(trigger_user_id)
    +'&message=' + encodeURIComponent(message)
    +'&notification_type=' + encodeURIComponent(notification_type)
    +'&reference_id=' + encodeURIComponent(reference_post_id);

    Data.post('notification',encodedCred).then(function(results)
  {
console.log("results from creating notification");
console.log(results.message);

    });
  }


  //CLICK IN USER NAME
  $scope.navigateToUserProfile = function(user){

      GooglePlacesService.callAnalyticsEvent("Click","Navigate to user profile from Reviews","Navigate to user profile from reviews");

      reviewsPopup.close();
    if(user.ismechanic){
    $state.go('app.profile.services', {userId: user.id});
  }else {
    {
      $state.go('app.profile.posts', {userId: user.id});
    }
  }
  };

})


.controller('editProfileCtrl', function($scope)
{

})


.controller('NewPostCtrl', function($scope, GooglePlacesService, $filter, $ionicModal, $ionicLoading, $timeout, $cordovaImagePicker,$cordovaFileTransfer, $ionicPlatform, GooglePlacesService, AuthService, Data, FeedService, $state, $ionicHistory, $ionicPopup, ProfileService) {

  $scope.job_post = {
    category: 'null',
    description: '',
    images: [],
    location: '',
    title: '',
    paid_option: 'null'
  };

$scope.imageChanged = false;

  $scope.service_post = {
    category: 'null',
    description: '',
    images: [],
    location: '',
    paid_option: 'null'
  };
$scope.oldphoto = '';
$scope.loggedUserIsMechanic = localStorage.getItem("ismechanic");


  $ionicModal.fromTemplateUrl('views/app/partials/new_job_post.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.new_job_post_modal = modal;
  });

  $ionicModal.fromTemplateUrl('views/app/partials/new_service_post.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.new_service_post_modal = modal;
  });

  $ionicModal.fromTemplateUrl('views/app/partials/checkin_job_post.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.checkin_job_post_modal = modal;
  });

  $ionicModal.fromTemplateUrl('views/app/partials/checkin_service_area.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.checkin_service_area_modal = modal;
  });

  $ionicModal.fromTemplateUrl('views/app/partials/search.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.search_modal = modal;
  });

  $ionicModal.fromTemplateUrl('views/app/partials/buycredit.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.buycredit_modal = modal;
  });

  $ionicModal.fromTemplateUrl('views/app/profile/editprofile.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.editprofile_modal = modal;
  });

  $ionicModal.fromTemplateUrl('views/app/profile/changepassword.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.changepassword_modal = modal;
  });

  $scope.showChangePasswordModal = function()
  {
    $scope.changepassword_modal.show();
  }

  $scope.closeChangePasswordModal = function()
  {
    $scope.changepassword_modal.hide();
  }

  $scope.showEditProfileModal = function()
  {
    $scope.updateEmail = true;
    $scope.updatePhone = true;
    $scope.updateEmail = function()
    {
    if(confirm("Account sign in email will be changed too.")){  $scope.updateEmail = false};
      console.log('allow update email');
    }
    $scope.updatePhone = function()
    {
      $scope.updatePhone = false;
      console.log('allow update phone');
    }

    //Fetch the user's data from database
    Data.get('userinfo').then(function(results)
    {
      $scope.loggedUserData = results;
      console.log(results);

    })
    $scope.editprofile_modal.show();


  }

  $scope.trustUser = function(user)
  {

    var userdata = user;


    Data.post('trust/'+user.id).then(function(results)
  {
    if(!results.error)
    {
      $scope.createNotification(user.id, localStorage.getItem("userId"), 'You have been trusted', 4, localStorage.getItem("userId"),'true');
    }
    console.log(results);
$scope.user.followers ="";
    if(!results.error){$scope.currentUserIsTrusted = true;
      Data.get('trust/'+user.id).then(function(results)
    {
      //var dd = results.user_data.id;
      //var following = {
        //userId:dd,
        //userData: results.trusted_user_data,
      //};
      $scope.user.followers = results;
      console.log("new followers data");
      console.log($scope.user.followers);


    })};
  });
  }

  //Create a new notification for action
  //notification_type :the type of notification, i.e: bid -1, comment -2, review -3, trusted -4,
  $scope.createNotification = function(for_user_id, trigger_user_id, message, notification_type, reference_post_id)
  {
    var encodedCred = 'for_user_id=' + encodeURIComponent(for_user_id)
    +'&trigger_user_id=' + encodeURIComponent(trigger_user_id)
    +'&message=' + encodeURIComponent(message)
    +'&notification_type=' + encodeURIComponent(notification_type)
    +'&reference_id=' + encodeURIComponent(reference_post_id);

    Data.post('notification',encodedCred).then(function(results)
  {
console.log("results from creating notification");
console.log(results.message);

    });
  }






  $scope.unTrustUser = function(user)
  {

    var userdata = user;
    Data.delete('trusted/'+user.id).then(function(results)
  {
    console.log(results);
$scope.user.followers ="";
    if(!results.error){$scope.currentUserIsTrusted = false;
      Data.get('trust/'+user.id).then(function(results)
    {
      //var dd = results.user_data.id;
      //var following = {
        //userId:dd,
        //userData: results.trusted_user_data,
      //};
      $scope.user.followers = results;
      console.log("new followers data");
      console.log($scope.user.followers);


    })};
  });
  }




  $scope.updateProfile = function()
  {
    var encodedCred =
								 'fname=' + encodeURIComponent($scope.loggedUserData.fname) +
								 '&lname=' + encodeURIComponent($scope.loggedUserData.lname) +
								 '&email=' + encodeURIComponent($scope.loggedUserData.email) +
                 '&address=' + encodeURIComponent($scope.loggedUserData.address) +
								 '&phone=' + encodeURIComponent($scope.loggedUserData.phone) +
								 '&about=' + encodeURIComponent($scope.loggedUserData.about) +
								 '&website=' + encodeURIComponent($scope.loggedUserData.website);

                 if($scope.imageChanged)
                 {
                   console.log("Uploading prifile image");
                   $scope.uploadProfileImage();
                 }

  Data.put('updateprofile',encodedCred).then(function (results) {
    $ionicLoading.show({
      template: 'Updating profile...'
    });
          console.log(results);
            if(results.error)
            {
              $ionicLoading.hide();
                console.log(results);
            }
            else {
            Data.toast("success","Profile Update",results.message)
            $ionicLoading.hide();
            $scope.closeEditProfileModal();
              console.log("Profile update Successfully :"+results.message);
            }
      });

  }

  $scope.closeEditProfileModal = function()
  {
    $scope.editprofile_modal.hide();
  }

  $scope.showBuyCreditModal = function()
  {
    $scope.buycredit_modal.show();
  }

  $scope.closeBuyCreditModal = function()
  {
  GooglePlacesService.callAnalyticsEvent("Click","Buy credit modal closed","Buy credit modal closed");

    $scope.buycredit_modal.hide();
  }

  $scope.newJobPost = function() {
    $scope.new_job_post_modal.show();
  };

  $scope.editJobPost = function(post) {

    $scope.job_post = {
      category: post.category.id,
      description: post.description,
      images: [post.photo],
      location: post.location,
      fulllocation: post.location,
      title: post.title,
      paid_option: 'null',
      editmode: 'on'
    };
    $scope.new_job_post_modal.show();
  };

  $scope.newServicePost = function() {
    $scope.new_service_post_modal.show();
  };

  $scope.editServicePost = function(service) {
    $scope.service_post = {
      title: service.title,
      category: service.category.id,
      description: service.description,
      images: [service.photo],
      location: service.location,
      fulllocation: service.location,
      paid_option: 'null',
      editmode: 'on'
    };
    $scope.new_service_post_modal.show();
  };

  $scope.newImageJobPost = function() {
    $scope.new_job_post_modal.show();
    $scope.openImagePicker();
  };

  $scope.newImageServicePost = function() {
    $scope.new_service_post_modal.show();
    $scope.openImagePicker();
  };

  $scope.openImagePicker = function(){
    //We use image picker plugin: http://ngcordova.com/docs/plugins/imagePicker/
    //implemented for iOS and Android 4.0 and above.
if($scope.loggedUserData){$scope.oldphoto = $scope.loggedUserData.photo;};
if( $scope.job_post.images){$scope.oldphoto =$scope.job_post.images;};
if($scope.service_post.images){$scope.oldphoto =$scope.service_post.images};
    $ionicPlatform.ready(function() {
      var options = {
  maximumImagesCount: 1,
  width: 800,
  height: 800,
  quality: 80
 };
      $cordovaImagePicker.getPictures(options)
       .then(function (results) {
          for (var i = 0; i < results.length; i++) {
            console.log('Image URI: ' + results[i]);
            $scope.job_post.images.push(results[i]);
            $scope.service_post.images.push(results[i]);
            $scope.loggedUserData.photo =results[i];
            $scope.imageChanged = true;
          }
        }, function(error) {
          // error getting photos
        });
    });
  }


  $scope.removeImage = function(image) {
  	$scope.job_post.images = _.without($scope.job_post.images, image);
    $scope.service_post.images = _.without($scope.service_post.images, image);
  };



  $scope.closeJobPost = function() {
    $scope.new_job_post_modal.hide();
  };

  $scope.closeServicePost = function() {
    $scope.new_service_post_modal.hide();
  };


  $scope.closeCheckInModal = function() {

  GooglePlacesService.callAnalyticsEvent("Click","CheckInmodal closed","CheckInmodal closed");

      $scope.predictions = [];
    $scope.checkin_job_post_modal.hide();
    $scope.new_job_post_modal.show();
  };

  $scope.closeServiceAreaCheckInModal = function() {
    $scope.predictions = [];
    $scope.checkin_service_area_modal.hide();
    $scope.new_service_post_modal.show();
  };
  $scope.closeSearchModal = function() {
    $scope.searchterm_results = [];
    $scope.searchterm_input = "";
    $scope.search_modal.hide();
  };

  $scope.showSearchModal = function(){
    $scope.search_modal.show();
    $scope.search = { input: '' };
    $scope.searchmode = "jobs";
  };

  $scope.showJobSearchModal = function(){
    $scope.search_modal.show();
    $scope.searchterm_input = "";
    $scope.searchmode = "jobs";
  };

  $scope.showServicesSearchModal = function(){
    $scope.search_modal.show();
    $scope.searchterm_input = "";
    $scope.searchmode = "services";
  };

  $scope.showMechanicsSearchModal = function(){
    $scope.search_modal.show();
    $scope.search = { input: '' };
    $scope.searchmode = "mechanics";
  };

  $scope.showPostNewService = function(){
    $scope.new_service_post_modal.show();
  };

  $scope.checkinJobPost = function(){
    $scope.new_job_post_modal.hide();
    $scope.checkin_job_post_modal.show();
    $scope.search = { input: '' };
  };

  $scope.checkinServiceArea = function(){
    $scope.new_service_post_modal.hide();
    $scope.checkin_service_area_modal.show();
    $scope.search = { input: '' };
  };

  $scope.getPlacePredictions = function(query){
    if(query !== "")
    {
      GooglePlacesService.getPlacePredictions(query)
      .then(function(predictions){
        $scope.predictions = predictions;
      });
    }else{
      $scope.predictions = [];
    }
  };

  $scope.searchForTerm = function(searchword, resultPage)
  {
    $scope.searchword = searchword;
    if(searchword !== "" && searchword.length >= 2)
    {

        console.log("searchword");
        console.log(searchword);
          var pageSize = 10, // Number of result records returned at once
              skip = pageSize * (resultPage-1),
              totalSearchResults = 1,
              searchterm_results = "",
              totalSearchPages = 1
              searchmode = $scope.searchmode + "earch";

              $scope.resultPage = resultPage;

          var encodedCred =
                       'start=' + encodeURIComponent(skip) +
                       '&end=' + encodeURIComponent(skip + pageSize);

                       console.log("data to search");
                       console.log(encodedCred );
          Data.post(searchmode+'/'+searchword,encodedCred).then(function(results)
        {
          console.log(results.resultdata);
          searchterm_results = results.resultdata;
          $scope.searchterm_results = searchterm_results;
          totalSearchResults = results.resultdatacount;
          $scope.totalSearchResults = results.resultdatacount;
          totalSearchPages = totalSearchResults/pageSize;
          $scope.totalSearchPages = totalSearchPages;
        })


    }else {
      {
        $scope.searchterm_results = [];
      }
    }
  }


  $scope.selectMapSearchResult = function(result){

   GooglePlacesService.callAnalyticsEvent("Click","Map search result select","Map search result select");

      $scope.search.input = result.description;
    $scope.predictions = [];
    $scope.closeCheckInModal();
    $scope.job_post.fulllocation = result.description;
    $scope.new_job_post_modal.show();
    $scope.job_post.location = result.terms[0].value;

  };

  $scope.selectMapSearchResultServicePost = function(result){
    $scope.search.input = result.description;
    $scope.predictions = [];
    $scope.closeServiceAreaCheckInModal();
    $scope.service_post.fulllocation = result.description;
    $scope.new_service_post_modal.show();
    $scope.service_post.location = result.terms[0].value;

  };

  $scope.selectSearchResult = function(result){

    $scope.searchterm_results = [];
    $scope.searchterm_input ="";
    $scope.closeSearchModal();
    if($scope.searchmode == "jobs")
    {
    $state.go('app.post', {postId: result.id});
    }
    else {
      $state.go('app.servicepost', {postId: result.id});
    }

  };


  $scope.loadMoreDataForSearchTermResult = function(){
var searchword = $scope.searchword;
    $scope.resultPage += 1;
    console.log(  $scope.searchword);
  //get more search result from db
  var pageSize = 10, // number of record per page
      skip = pageSize * ($scope.resultPage-1),
      searchmode = $scope.searchmode+"earch";

  var encodedCred =
               'start=' + encodeURIComponent(skip) +
               '&end=' + encodeURIComponent(skip + pageSize);

              console.log("searchmode");
              console.log(searchmode);
              console.log($scope.searchword);
               Data.post(searchmode+'/'+$scope.searchword,encodedCred).then(function(results)
             {
              $scope.totalSearchPages = results.resultdatacount/pageSize;
              $scope.searchterm_results = $scope.searchterm_results.concat(results.resultdata);
              console.log(results.resultdata);
             });
             $scope.$broadcast('scroll.infiniteScrollComplete');

  };

  $scope.moreDataCanBeLoadedSearchResult = function(){
    return $scope.totalSearchPages > $scope.resultPage;
  };


  //Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.new_job_post_modal.remove();
    $scope.new_service_post_modal.remove();
  });

  // Returns the local path inside the app for an image
  $scope.pathForImage = function(image) {
    if (image === null) {
      return '';
    } else {
      return cordova.file.dataDirectory + image;
    }
  };

  $scope.postJob = function() {



    var encodedCred =
								 'category_id=' + encodeURIComponent($scope.job_post.category) +
								 '&title=' + encodeURIComponent($scope.job_post.title) +
								 '&description=' + encodeURIComponent($scope.job_post.description) +
                 '&photo=' + encodeURIComponent($scope.photolink) +
								 '&location=' + encodeURIComponent($scope.job_post.fulllocation) +
								 '&paidoption=' + encodeURIComponent($scope.job_post.paid_option);


    Data.post('job',encodedCred).then(function (results) {

            console.log(results);
              if(results.error)
              {
                Data.toast("error","Job Posting",results.message);

                  console.log(results);
              }
              else {


                if(results.paid_option)
                {
                    Data.toast("success","Paid Services",results.paid_option);
                };
                Data.toast("success","Job Posted",results.message);

              $scope.closeJobPost();
                console.log("Job posted Successfully :"+results.message);
                //update jobfeed

              // $timeout(function() {
                //Data.refresh();
                $state.go('app.post', {postId: results.job_id});

            //}, 3000);
          }
        });

  };


  $scope.updateJob = function(post) {

    var encodedCred =
								 'category_id=' + encodeURIComponent($scope.job_post.category) +
								 '&title=' + encodeURIComponent($scope.job_post.title) +
								 '&description=' + encodeURIComponent($scope.job_post.description) +
                 '&photo=' + encodeURIComponent($scope.photolink) +
								 '&location=' + encodeURIComponent($scope.job_post.fulllocation) ;
							//	+ '&paidoption=' + encodeURIComponent($scope.job_post.paid_option);

    Data.put('job/'+post.id,encodedCred).then(function (results) {

            console.log(results);
              if(results.error)
              {
                Data.toast("error","Job Updating",results.message);

                  console.log(results);
              }
              else {


                if(results.paid_option){Data.toast("success","Paid Services",results.paid_option);};
                Data.toast("success","Job Updated",results.message);

              $scope.closeJobPost();
                console.log("Job updated Successfully :"+results.message);

              /*  Data.get('getjobpost/'+post.id).then(function(results)
              {
                 post = results;
                $scope.post = results;
                console.log(post);
              });*/
              Data.refresh();

          }
        });

  };

  $scope.deleteJobPost = function(post) {

    // Confirm before deleting

        var confirmPopup = $ionicPopup.confirm({
          title: 'Delete Job',
          template: 'Are you sure you want to delete this job <strong>"'+post.title+'"</strong>?'
        });
        confirmPopup.then(function(res) {
          if(res) {
            console.log('Yes delete job confirmed');
            console.log("Deleting job:"+post.title);

                Data.delete('job/'+post.id).then(function (results) {

                        console.log(results);
                          if(results.error)
                          {
                          //If error, do nothin

                              console.log(results);
                          }
                          else {
                            Data.toast("success","Job Delete",results.message);

                            console.log("Job deleted Successfully :"+results.message);
                            // Do something to load your new data here


                           $ionicHistory.currentView($ionicHistory.backView());
                            $ionicHistory.nextViewOptions({ disableAnimate: true });
                            $state.go('app.profile.posts', {userId: post.user.id});

                      }
                    });
          } else {
            console.log("No don't delete job confirmed");
          }
        });




  };



  $scope.genRandom = function() {
    return Math.floor(Date.now() / 1000);
  };

  $scope.uploadImageJob = function() {
    if(!$scope.job_post.images || $scope.job_post.images ==""){$scope.photolink = "img/defaultprofilephoto.jpg"; $scope.postJob();} else {
    $ionicLoading.show({
        content: '<i class="icon ion-loading-c"></i>',
        animation: 'fade-in',
        showBackdrop: false,
        maxWidth: 50,
        showDelay: 0
      });
      document.addEventListener("deviceready", onDeviceReady, false);
      $scope.photolink = "http://mechanlink.co.uk/api/include/imageuploads/"+ $scope.filename;
    // Destination URL
    var image_server = encodeURI("http://mechanlink.co.uk/api/include/imageupload.php");

    // File for Upload
    //var targetPath = "file:///data/data/com.cosesano.MechanLink/cache/tmp_Screenshot_2017-09-16-04-35-06-1748856664.png";

    // File name only
  //  var filename = $scope.job_post.images;
  //console.log("filename");
  //console.log(filename);

   // use file transfer after onDeviceReady() was called
  function onDeviceReady() {

       /** $cordovaFileTransfer.upload(url, targetPath, options).then(function(result) {
        $scope.showAlert('Success', 'Image upload finished. '+result);
        done = result;
      }); **/

  for (i=0; i<$scope.job_post.images.length; i=i+1)
  {
    // File name only
   $scope.filename = $scope.genRandom()+$scope.job_post.images[i].substr($scope.job_post.images[i].lastIndexOf('/')+1);

   filePath = $scope.job_post.images[i];
   /**var options = {
     fileKey: "file",
     fileName: filename,
     chunkedMode: true,
     method: 'POST',
     mimeType: "multipart/form-data",
     params : {'fileName': filename}
   };
  **/

   var win = function (r) {
      console.log("Code = " + r.responseCode);
      console.log("Response = " + r.response);
      console.log("Sent = " + r.bytesSent);
      console.log("Posting job now");
      Data.toast("success", "Post Photo", "Photo uploaded successfully");
      $scope.postJob();
    $ionicLoading.hide();
  }

  var fail = function (error) {
      console.log("upload error source " + error.source);
      console.log("upload error target " + error.target);
      Data.toast("error", "Post Photo", "Could not upload photo, try again");
      $ionicLoading.hide();
  }

  //var ft = new FileTransfer();
  //$cordovaFileTransfer.upload(image_server, filePath, options, win, fail )
  var myImg = filePath;
          var options = new FileUploadOptions();
          options.fileKey="file";
          options.chunkedMode = false;
          options.method = 'POST';
          options.fileName =  $scope.filename;
          var params = {};
          params.fileName=  $scope.filename;
          options.params = params;
          var ft = new FileTransfer();

          ft.upload(myImg, encodeURI("http://mechanlink.co.uk/api/include/imageupload.php"), win, fail, options);
  }
  }




    }

}

$scope.uploadImageUpdateJob = function(post) {

  if(!$scope.job_post.images || $scope.job_post.images ==""){$scope.photolink = "img/defaultprofilephoto.jpg"; $scope.updateJob(post);}
  else if(!$scope.imageChanged){ $scope.updateJob(post);$scope.photolink = $scope.job_post.images}
  else {
  $ionicLoading.show({
      content: '<i class="icon ion-loading-c"></i>',
      animation: 'fade-in',
      showBackdrop: false,
      maxWidth: 50,
      showDelay: 0
    });
    document.addEventListener("deviceready", onDeviceReady, false);
    $scope.photolink = "http://mechanlink.co.uk/api/include/imageuploads/"+ $scope.filename;
  // Destination URL
  var image_server = encodeURI("http://mechanlink.co.uk/api/include/imageupload.php");

  // File for Upload
  //var targetPath = "file:///data/data/com.cosesano.MechanLink/cache/tmp_Screenshot_2017-09-16-04-35-06-1748856664.png";

  // File name only
//  var filename = $scope.job_post.images;
//console.log("filename");
//console.log(filename);

 // use file transfer after onDeviceReady() was called
function onDeviceReady() {

     /** $cordovaFileTransfer.upload(url, targetPath, options).then(function(result) {
      $scope.showAlert('Success', 'Image upload finished. '+result);
      done = result;
    }); **/

for (i=0; i<$scope.job_post.images.length; i=i+1)
{
  // File name only
 $scope.filename = $scope.genRandom()+$scope.job_post.images[i].substr($scope.job_post.images[i].lastIndexOf('/')+1);

 filePath = $scope.job_post.images[i];
 /**var options = {
   fileKey: "file",
   fileName: filename,
   chunkedMode: true,
   method: 'POST',
   mimeType: "multipart/form-data",
   params : {'fileName': filename}
 };
**/

 var win = function (r) {
    console.log("Code = " + r.responseCode);
    console.log("Response = " + r.response);
    console.log("Sent = " + r.bytesSent);
    console.log("Updating job now");
    Data.toast("success", "Post Photo", "Photo updated successfully");
    $scope.updateJob(post);
    //delete old photo
    console.log("deleting old photo");
    console.log($scope.oldphoto.substr($scope.oldphoto.lastIndexOf('/')+1));
    Data.delete('deletefile/'+$scope.oldphoto,).then(function(results)
  {
    console.log(results);
    if(!results.error)
    {
          console.log("deletd old photo");
    }
    else {
        console.log("error while deleting old photo");
    }

  })
  $ionicLoading.hide();
}

var fail = function (error) {
    console.log("upload error source " + error.source);
    console.log("upload error target " + error.target);
    Data.toast("error", "Post Photo", "Could not update photo, try again");
    $ionicLoading.hide();
}

//var ft = new FileTransfer();
//$cordovaFileTransfer.upload(image_server, filePath, options, win, fail )
var myImg = filePath;
        var options = new FileUploadOptions();
        options.fileKey="file";
        options.chunkedMode = false;
        options.method = 'POST';
        options.fileName =  $scope.filename;
        var params = {};
        params.fileName=  $scope.filename;
        options.params = params;
        var ft = new FileTransfer();

        ft.upload(myImg, encodeURI("http://mechanlink.co.uk/api/include/imageupload.php"), win, fail, options);
}
}




  }

}



$scope.uploadImageService = function() {
  if(!$scope.service_post.images|| $scope.service_post.images==""){$scope.photolink = "img/defaultprofilephoto.jpg"; $scope.postService();} else {
  $ionicLoading.show({
      content: '<i class="icon ion-loading-c"></i>',
      animation: 'fade-in',
      showBackdrop: false,
      maxWidth: 50,
      showDelay: 0
    });
    document.addEventListener("deviceready", onDeviceReady, false);
    $scope.photolink = "http://mechanlink.co.uk/api/include/imageuploads/"+ $scope.filename;
  // Destination URL
  var image_server = encodeURI("http://mechanlink.co.uk/api/include/imageupload.php");

  // File for Upload
  //var targetPath = "file:///data/data/com.cosesano.MechanLink/cache/tmp_Screenshot_2017-09-16-04-35-06-1748856664.png";

  // File name only
//  var filename = $scope.job_post.images;
//console.log("filename");
//console.log(filename);

 // use file transfer after onDeviceReady() was called
function onDeviceReady() {

     /** $cordovaFileTransfer.upload(url, targetPath, options).then(function(result) {
      $scope.showAlert('Success', 'Image upload finished. '+result);
      done = result;
    }); **/

for (i=0; i<$scope.service_post.images.length; i=i+1)
{
  // File name only
 $scope.filename = $scope.genRandom()+$scope.service_post.images[i].substr($scope.service_post.images[i].lastIndexOf('/')+1);

 filePath = $scope.service_post.images[i];
 /**var options = {
   fileKey: "file",
   fileName: filename,
   chunkedMode: true,
   method: 'POST',
   mimeType: "multipart/form-data",
   params : {'fileName': filename}
 };
**/

 var win = function (r) {
    console.log("Code = " + r.responseCode);
    console.log("Response = " + r.response);
    console.log("Sent = " + r.bytesSent);
    console.log("Posting job now");
    Data.toast("success", "Post Photo", "Photo uploaded successfully");
    $scope.postService();
  $ionicLoading.hide();
}

var fail = function (error) {
    console.log("upload error source " + error.source);
    console.log("upload error target " + error.target);
    Data.toast("error", "Post Photo", "Could not upload photo, try again");
    $ionicLoading.hide();
}

//var ft = new FileTransfer();
//$cordovaFileTransfer.upload(image_server, filePath, options, win, fail )
var myImg = filePath;
        var options = new FileUploadOptions();
        options.fileKey="file";
        options.chunkedMode = false;
        options.method = 'POST';
        options.fileName =  $scope.filename;
        var params = {};
        params.fileName=  $scope.filename;
        options.params = params;
        var ft = new FileTransfer();

        ft.upload(myImg, encodeURI("http://mechanlink.co.uk/api/include/imageupload.php"), win, fail, options);
}
}




  }

}


$scope.uploadProfileImage= function() {

  $ionicLoading.show({
      content: '<i class="icon ion-loading-c"></i>',
      animation: 'fade-in',
      showBackdrop: false,
      maxWidth: 50,
      showDelay: 0
    });
    document.addEventListener("deviceready", onDeviceReady, false);
    $scope.photolink = "http://mechanlink.co.uk/api/include/imageuploads/"+ $scope.filename;
  // Destination URL
  var image_server = encodeURI("http://mechanlink.co.uk/api/include/imageupload.php");


 // use file transfer after onDeviceReady() was called
function onDeviceReady() {


  // File name only
 $scope.filename = $scope.genRandom()+$scope.loggedUserData.photo.substr($scope.loggedUserData.photo.lastIndexOf('/')+1);

 filePath = $scope.loggedUserData.photo;
console.log("normal path");
console.log($scope.loggedUserData.photo);
 var win = function (r) {
    console.log("Code = " + r.responseCode);
    console.log("Response = " + r.response);
    console.log("Sent = " + r.bytesSent);
    console.log("Changing profile picture");


    //update profile picture in DB
    var encodedCred =
								 'photo=' + encodeURIComponent($scope.photolink);
								 console.log(encodedCred );
		Data.put('changeprofilepicture',encodedCred).then(function(results)
	{
		console.log(results);
		if(!results.error)
		{
			  Data.toast("success", "Profile Picture", "Profile picture changed successfully");
		}
		else {
			Data.toast("error", "Profile picture", "Could not upload photo, try again");
		}

	})

  //delete old photo
  console.log("deleting old photo");
  console.log($scope.oldphoto.substr($scope.oldphoto.lastIndexOf('/')+1));
  Data.delete('deletefile/'+$scope.oldphoto,).then(function(results)
{
  console.log(results);
  if(!results.error)
  {
        console.log("deletd old photo");
  }
  else {
      console.log("error while deleting old photo");
  }

})
  $ionicLoading.hide();
}

var fail = function (error) {
    console.log("upload error source " + error.source);
    console.log("upload error target " + error.target);
    Data.toast("error", "Profile picture", "Could not upload photo, try again");
    $ionicLoading.hide();
}

var myImg = filePath;
        var options = new FileUploadOptions();
        options.fileKey="file";
        options.chunkedMode = false;
        options.method = 'POST';
        options.fileName =  $scope.filename;
        var params = {};
        params.fileName=  $scope.filename;
        options.params = params;
        var ft = new FileTransfer();

        ft.upload(myImg, encodeURI("http://mechanlink.co.uk/api/include/imageupload.php"), win, fail, options);


}




  }




  $scope.postService = function() {

      var encodedCred =
  								 'category_id=' + encodeURIComponent($scope.service_post.category) +
  								 '&title=' + encodeURIComponent($scope.service_post.title) +
  								 '&description=' + encodeURIComponent($scope.service_post.description) +
                   '&photo=' + encodeURIComponent($scope.photolink) +
  								 '&location=' + encodeURIComponent($scope.service_post.fulllocation);
  console.log("Data to post:"+encodedCred);
    Data.post('service',encodedCred).then(function (results) {

            console.log(results);
              if(results.error)
              {
                Data.toast("error","Service Posting",results.message);

                  console.log(results);
              }
              else {


                //if(results.paid_option){Data.toast("success","Paid Services",results.paid_option);};
                Data.toast("success","Service Posted",results.message);
              //$ionicLoading.hide();
              $scope.closeServicePost();
                console.log("Service posted Successfully :"+results.message);


              // $timeout(function() {
                $state.go('app.servicepost', {postId: results.service_id});
            //}, 3000);
          }
        });
      };


      $scope.uploadImageUpdateService = function(service) {

        if(!$scope.service_post.images || $scope.service_post.images ==""){$scope.photolink = "img/defaultprofilephoto.jpg"; $scope.updateService(service);}
        else if(!$scope.imageChanged){ $scope.updateService(service);$scope.photolink = $scope.service_post.images}
        else {
        $ionicLoading.show({
            content: '<i class="icon ion-loading-c"></i>',
            animation: 'fade-in',
            showBackdrop: false,
            maxWidth: 50,
            showDelay: 0
          });
          document.addEventListener("deviceready", onDeviceReady, false);
          $scope.photolink = "http://mechanlink.co.uk/api/include/imageuploads/"+ $scope.filename;
        // Destination URL
        var image_server = encodeURI("http://mechanlink.co.uk/api/include/imageupload.php");

        // File for Upload
        //var targetPath = "file:///data/data/com.cosesano.MechanLink/cache/tmp_Screenshot_2017-09-16-04-35-06-1748856664.png";

        // File name only
      //  var filename = $scope.job_post.images;
      //console.log("filename");
      //console.log(filename);

       // use file transfer after onDeviceReady() was called
      function onDeviceReady() {

           /** $cordovaFileTransfer.upload(url, targetPath, options).then(function(result) {
            $scope.showAlert('Success', 'Image upload finished. '+result);
            done = result;
          }); **/

      for (i=0; i<$scope.service_post.images.length; i=i+1)
      {
        // File name only
       $scope.filename = $scope.genRandom()+$scope.service_post.images[i].substr($scope.service_post.images[i].lastIndexOf('/')+1);

       filePath = $scope.service_post.images[i];
       /**var options = {
         fileKey: "file",
         fileName: filename,
         chunkedMode: true,
         method: 'POST',
         mimeType: "multipart/form-data",
         params : {'fileName': filename}
       };
      **/

       var win = function (r) {
          console.log("Code = " + r.responseCode);
          console.log("Response = " + r.response);
          console.log("Sent = " + r.bytesSent);
          console.log("Updating job now");
          Data.toast("success", "Post Photo", "Photo updated successfully");
          $scope.updateService(service);
          //delete old photo
          console.log("deleting old photo");
          console.log($scope.oldphoto.substr($scope.oldphoto.lastIndexOf('/')+1));
          Data.delete('deletefile/'+$scope.oldphoto,).then(function(results)
        {
          console.log(results);
          if(!results.error)
          {
                console.log("deletd old photo");
          }
          else {
              console.log("error while deleting old photo");
          }

        })
        $ionicLoading.hide();
      }

      var fail = function (error) {
          console.log("upload error source " + error.source);
          console.log("upload error target " + error.target);
          Data.toast("error", "Post Photo", "Could not update photo, try again");
          $ionicLoading.hide();
      }

      //var ft = new FileTransfer();
      //$cordovaFileTransfer.upload(image_server, filePath, options, win, fail )
      var myImg = filePath;
              var options = new FileUploadOptions();
              options.fileKey="file";
              options.chunkedMode = false;
              options.method = 'POST';
              options.fileName =  $scope.filename;
              var params = {};
              params.fileName=  $scope.filename;
              options.params = params;
              var ft = new FileTransfer();

              ft.upload(myImg, encodeURI("http://mechanlink.co.uk/api/include/imageupload.php"), win, fail, options);
      }
      }




        }

      }



      $scope.updateService = function(service) {


          //if(!$scope.service_post.images || $scope.service_post.images ==""){$scope.service_post.images = "img/defaultprofilephoto.jpg"};
          var encodedCred =
                       'category_id=' + encodeURIComponent($scope.service_post.category) +
                       '&title=' + encodeURIComponent($scope.service_post.title) +
                       '&description=' + encodeURIComponent($scope.service_post.description) +
                       '&photo=' + encodeURIComponent($scope.photolink) +
                       '&location=' + encodeURIComponent($scope.service_post.fulllocation);
      console.log("Data to post:"+encodedCred);
        Data.put('service/'+service.id,encodedCred).then(function (results) {

                console.log(results);
                  if(results.error)
                  {
                    Data.toast("error","Service updating",results.message);

                      console.log(results);
                  }
                  else {


                    //if(results.paid_option){Data.toast("success","Paid Services",results.paid_option);};
                    Data.toast("success","Service Updated",results.message);
                  //$ionicLoading.hide();
                  $scope.closeServicePost();
                    console.log("Service updated Successfully :"+results.message);


                Data.refresh();
              }
            });
          };

          $scope.deleteServicePost = function(service) {

            // Confirm before deleting

                var confirmPopup = $ionicPopup.confirm({
                  title: 'Delete Service',
                  template: 'Are you sure you want to delete this service <strong>"'+service.title+'"</strong>?'
                });
                confirmPopup.then(function(res) {
                  if(res) {
                    console.log('Yes delete service confirmed');
                    console.log("Deleting service:"+service.title);

                        Data.delete('service/'+service.id).then(function (results) {

                                console.log(results);
                                  if(results.error)
                                  {
                                  //If error, do nothin

                                      console.log(results);
                                  }
                                  else {
                                    Data.toast("success","Service Delete",results.message);

                                    console.log("service deleted Successfully :"+results.message);


                                    $ionicHistory.currentView($ionicHistory.backView());
                                    $ionicHistory.nextViewOptions({ disableAnimate: true });
                                    $state.go('app.profile.services', {userId: service.user.id});

                              }
                            });
                  } else {
                    console.log("No don't delete service confirmed");
                  }
                });




          };




})

.controller('notificationCtrl', function($scope, Data, $state)
{
  $scope.lt = "";
  //Remove a notification
  $scope.removeNotification = function(notification)
  {

    Data.delete('notification/'+notification.id).then(function(results)
  {
console.log("results from remoing notification");
console.log(results.message);
if(!results.error){$scope.notifications.splice($scope.notifications.indexOf(notification), 1)

  Data.get('notification/new').then(function(results)
{

  console.log("user new notification count from notificationCtrl");
  console.log(results);
  $scope.newnotificationscount = results;
});

Data.get('notification/all').then(function(results)
{

console.log("user notification result");
console.log(results);
$scope.notifications = results;
$scope.lt = results.length;
});
};

    });
  }

  //Remove all notification
  $scope.removeAllNotification = function()
  {

    Data.delete('notification/all/'+localStorage.getItem("userId")).then(function(results)
  {
console.log("results from removing all notifications");
console.log(results.message);
if(!results.error){$scope.notifications.length = 0};

    });
  };
  //CLICK IN USER NAME
  $scope.navigateToUserProfile = function(user){
    if(user.ismechanic){
    $state.go('app.profile.services', {userId: user.id});
  }else {
    {
      $state.go('app.profile.posts', {userId: user.id});
    }
  }
  };
})


.controller('CategoryFeedCtrl', function($scope, _, FeedService, $stateParams, loggedUser, feed, category,newnotificationscount, notifications) {
  $scope.loggedUser = loggedUser;
  $scope.cards = feed.posts;
  $scope.current_category = category;
  $scope.newnotificationscount = newnotificationscount;
  $scope.notifications = notifications;
  $scope.page = 1;// Default page is 1
  $scope.totalPages = feed.totalPages;


  var categoryId = $stateParams.categoryId;

  $scope.is_category_feed = true;

  $scope.getNewData = function() {

    $scope.$broadcast('scroll.refreshComplete');
    FeedService.getFeedByCategory(1, categoryId)
    .then(function(data){
      $scope.totalPages = data.totallivejobsincategory;
      $scope.cards = data.posts;
    });
  };

  $scope.loadMoreDataForJobCategory = function(){
    $scope.page += 1;
//get more job feed from db
    FeedService.getFeedByCategory($scope.page, categoryId)
    .then(function(data){
      //We will update this value in every request because new posts can be created
      $scope.totalPages = data.totallivejobsincategory;
      $scope.cards = $scope.cards.concat(data.posts);
      console.log("Adding new job feed data based on category");
      console.log(data.posts);
      $scope.$broadcast('scroll.infiniteScrollComplete');
    });
  };

  $scope.moreDataCanBeLoadedForCategory = function(){
    return $scope.totalPages > $scope.page;
};

})


.controller('ServiceCategoryFeedCtrl', function($scope, _, FeedService, $stateParams, loggedUser, servicefeed, category, newnotificationscount, notifications) {
  $scope.loggedUser = loggedUser;
  $scope.cards = servicefeed.serviceposts;
  $scope.current_category = category;
  $scope.newnotificationscount = newnotificationscount;
  $scope.notifications = notifications;
  $scope.page = 1;// Default page is 1
  $scope.totalPages = servicefeed.totalPages;


  var categoryId = $stateParams.categoryId;

  $scope.is_category_feed = true;

  $scope.getNewServiceData = function() {
    $scope.$broadcast('scroll.refreshComplete');
    FeedService.getServiceFeedByCategory(1, categoryId)
    .then(function(data){
      $scope.totalPages = data.totalservicesincategory;
      $scope.cards = data.serviceposts;
    });
  };

  $scope.loadMoreDataForServiceCategory = function(){
    $scope.page += 1;
//get more job feed from db
    FeedService.getServiceFeedByCategory($scope.page, categoryId)
    .then(function(data){
      //We will update this value in every request because new posts can be created
      $scope.totalPages = data.totalservicesincategory;
      $scope.cards = $scope.cards.concat(data.serviceposts);
      console.log("Adding new service feed data based on category");
      console.log(data.serviceposts);
      $scope.$broadcast('scroll.infiniteScrollComplete');
    });
  };

  $scope.moreDataCanBeLoadedForServiceCategory = function(){
    return $scope.totalPages > $scope.page;


  };
})




.controller('TrendFeedCtrl', function($scope, _, FeedService, $stateParams, loggedUser, feed, trend) {
  $scope.loggedUser = loggedUser;
  $scope.cards = feed.posts;
  $scope.current_trend = trend;

  $scope.page = 1;// Default page is 1
  $scope.totalPages = feed.totalPages;

  // Check if we are loading posts from one category or trend
  var trendId = $stateParams.trendId;

  $scope.is_trend_feed = true;

  $scope.getNewData = function() {
    // Do something to load your new data here
    $scope.$broadcast('scroll.refreshComplete');

  };

  $scope.loadMoreData = function(){
    $scope.page += 1;

    console.log("Get trends feed");
    // get trend feed
    FeedService.getFeedByTrend($scope.page, trendId)
    .then(function(data){
      //We will update this value in every request because new posts can be created
      $scope.totalPages = data.totalPages;
      $scope.cards = $scope.cards.concat(data.posts);

      $scope.$broadcast('scroll.infiniteScrollComplete');
    });
  };

  $scope.moreDataCanBeLoaded = function(){
    return $scope.totalPages > $scope.page;
  };
})

.controller('FeedCtrl', function(Data, $ionicScrollDelegate, $state, $scope, _, FeedService, $stateParams, loggedUser, feed, activeJobs, $location, $anchorScroll) {

  $scope.currentUserId = loggedUser.id;
  $scope.loggedUser = loggedUser;
  $scope.cards = feed.posts;
  $scope.activeJobsCards = activeJobs;
  $scope.page = 1;// Default page is 1
  $scope.totalPages = feed.totalPages;
  $scope.is_category_feed = false;
  $scope.is_trend_feed = false;
$scope.lt = "";
var currentPostCount = feed.totalPosts;
var newPostCount =0;




setInterval(function(){
  console.log("current feed count");
console.log(currentPostCount);
//get new post count
var pageSize = 5,
    skip = 0,
 encodedCred =
             'start=' + encodeURIComponent(skip) +
             '&end=' + encodeURIComponent(skip + pageSize);

Data.post('getjobfeed', encodedCred).then(function(results)
{
      newPostCount = results.totallivejobs;
      console.log("new feed count");
      console.log(newPostCount);
      if(newPostCount > currentPostCount)
      {
        $scope.newPostAvailable = true;
      }

});

//get new notification
Data.get('notification/new').then(function(results)
{

console.log("user new notification count from FeedCtrl");
console.log(results);
$scope.newnotificationscount = results;
});

Data.get('notification/all').then(function(results)
{

console.log("user notification result");
console.log(results);
$scope.notifications = results;
$scope.lt = results.length;
});

FeedService.getUserActiveJobs($scope.loggedUserId)
.then(function(data){
  console.log("Active jobs afer delete");
  console.log(data);
  $scope.activeJobsCards = data;
});

}, 120000)






  $scope.refreshFeed = function()
  {

    FeedService.getFeed(1)
    .then(function(data){
      //We will update this value in every request because new posts can be created
      $scope.totalPages = data.totalPages;
      $scope.cards =  data.posts;
      console.log("Add new feed data");
      console.log(data.posts);
      $scope.activeJobsCards = activeJobs;
$scope.newPostAvailable = false;
      $scope.scrollToTop();
      //$scope.newPostAvailable = true;
      currentPostCount = newPostCount;
      console.log("new feed count after pressing refreshing");
      console.log(currentPostCount);
    });
  }

  $scope.scrollToTop = function()
  {
    $location.hash('scrollTop');

     // call $anchorScroll()
     $anchorScroll();
  }

  $scope.getNewData = function() {

    // Do something to load your new data here
    $scope.$broadcast('scroll.refreshComplete');
    FeedService.getFeed(1)
    .then(function(data){
      $scope.totalPages = data.totalPages;
      $scope.cards = data.posts;
      console.log("Active jobs");
      console.log(activeJobs);
      $scope.activeJobsCards = activeJobs;
    });
  };

  $scope.loadMoreData = function(){
    $scope.page += 1;
//get more job feed from db
    FeedService.getFeed($scope.page)
    .then(function(data){
      //We will update this value in every request because new posts can be created
      $scope.totalPages = data.totalPages;
      $scope.cards = $scope.cards.concat(data.posts);
      console.log("Add new feed data");
      console.log(data.posts);
      $scope.$broadcast('scroll.infiniteScrollComplete');
    });
  };

  $scope.moreDataCanBeLoaded = function(){
    return $scope.totalPages > $scope.page;
  };
})

.controller('MechanicsCtrl', function($scope, mechanics_suggestions, sponsored_mechanics,Data) {
  $scope.mechanics_suggestions = mechanics_suggestions;
  $scope.sponsored_mechanics = sponsored_mechanics;
  $scope.mechanics_search_results = "";
  $scope.totalSearchPages = "";
  $scope.totalSearchResult = "";
  $scope.resultPage = "";
  var mechanics_search_results = [];

  $scope.searchForMechanics = function (searchword, resultPage)
  {
    console.log("searchword");
    console.log(searchword);
    if(searchword != "" && searchword != 'undefined' && searchword.length >= 2){
      var pageSize = 10, // set your page size, which is number of records per page
          skip = pageSize * (resultPage-1),
          totalSearchResults = 1,
          totalSearchPages = 1;

          $scope.resultPage = resultPage;

      var encodedCred =
                   'start=' + encodeURIComponent(skip) +
                   '&end=' + encodeURIComponent(skip + pageSize);

                   console.log("data to search");
                   console.log(encodedCred );
      Data.post('mechanicsearch/'+searchword,encodedCred).then(function(results)
    {
      console.log(results.resultdatacount);
      mechanics_search_results = results.resultdata;
      $scope.mechanics_search_results = mechanics_search_results;
      totalSearchResults = results.resultdatacount;
      $scope.totalSearchResults = results.resultdatacount;
      totalSearchPages = totalSearchResults/pageSize;
      $scope.totalSearchPages = totalSearchPages;
    })
    }

};

$scope.loadMoreDataForSearchResult = function(searchword){
  $scope.resultPage += 1;
//get more search result from db
var pageSize = 10, // number of record per page
    skip = pageSize * ($scope.resultPage-1);

var encodedCred =
             'start=' + encodeURIComponent(skip) +
             '&end=' + encodeURIComponent(skip + pageSize);

             Data.post('mechanicsearch/'+searchword,encodedCred).then(function(results)
           {
            $scope.totalSearchPages = results.resultdatacount/pageSize;
            $scope.mechanics_search_results = $scope.mechanics_search_results.concat(results.resultdata);
           });

};

$scope.moreDataCanBeLoadedSearchResult = function(){
  return $scope.totalSearchPages > $scope.resultPage;
};

})

.controller('BrowseCtrl', function($scope, trends, categories, loggedUser) {
  $scope.trends = trends;
  $scope.categories = categories;
  $scope.loggedUser = loggedUser;
})

.controller('AppCtrl', function($scope, $ionicModal, $timeout, AuthService) {
  // Form data for the login modal
  $scope.loginData = {};

  $ionicModal.fromTemplateUrl('views/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

})

.controller('EmailComposerCtrl', function($scope, $cordovaEmailComposer, $ionicPlatform) {
  //we use email composer cordova plugin, see the documentation for mor options: http://ngcordova.com/docs/plugins/emailComposer/
  $scope.sendMail = function(){
    $ionicPlatform.ready(function() {
      $cordovaEmailComposer.isAvailable().then(function() {
        // is available
        console.log("Is available");
        $cordovaEmailComposer.open({
          to: 'hi@startapplabs.com',
          subject: 'Nice Theme!',
          body: 'How are you? Nice greetings from Social App'
        }).then(null, function () {
          // user cancelled email
        });
      }, function () {
        // not available
        console.log("Not available");
      });
    });
  };
})

.controller('SettingsCtrl', function($scope, $ionicModal, Data) {

  $ionicModal.fromTemplateUrl('views/app/legal/terms-of-service.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.terms_of_service_modal = modal;
  });

  $ionicModal.fromTemplateUrl('views/app/legal/verify-account.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.verify_account_modal = modal;
  });

  $ionicModal.fromTemplateUrl('views/app/legal/privacy-policy.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.privacy_policy_modal = modal;
  });

  $scope.showVerifyAccount = function()
  {
      $scope.verify_account_modal.show();
  }

  $scope.showTerms = function() {
    $scope.terms_of_service_modal.show();
  };

  $scope.showPrivacyPolicy = function() {
    $scope.privacy_policy_modal.show();
  };
})

.controller('AppRateCtrl', function($scope) {
	$scope.appRate = function(){
		if(ionic.Platform.isIOS()){
			//you need to set your own ios app id
			AppRate.preferences.storeAppURL.ios = '1327505648';
			AppRate.promptForRating(true);
		}else if(ionic.Platform.isAndroid()){
			//you need to set your own android app id
			AppRate.preferences.storeAppURL.android = 'com.mechanlinkltd.mechanlink';
			AppRate.promptForRating(true);
		}
	};
})

.controller('PostDetailsCtrl', function($scope, post ) {
  //is this the profile of the logged user?
  $scope.post = post;
  console.log("post details");
  console.log(post);

})


.controller('GeoCtrl', function($cordovaGeolocation,$http, $rootScope) {
  ionic.Platform.ready(function(){
    var posOptions = {enableHighAccuracy: true,
            timeout: 20000,
            maximumAge: 0};
    $cordovaGeolocation
      .getCurrentPosition(posOptions)
      .then(function (position) {
        var lat  = position.coords.latitude //here you get latitude
        var long = position.coords.longitude //here you get the longitude
   $http.get('http://maps.googleapis.com/maps/api/geocode/json?latlng='+lat+','+long+'&sensor=true').then(function(data){
      $rootScope.CurrentLocation=data.data.results[0].formatted_address;//you get the current location here
      console.log(data);
      console.log("lat:"+lat);
      console.log("long:"+long);
      $rootScope.lat = lat;
      $rootScope.long = long;
          }, function(err) {
            // error
            console.log(err);
          });
      }, function(err) {
        // error
        console.log(err);
      });
    })


})

.controller('ServiceDetailsCtrl', function($scope, service ) {
  //is this the profile of the logged user?
  $scope.service = service;

})
