angular.module('mechanlink.auth.controllers', ['toaster'])


.controller('WelcomeCtrl', function($scope, $state, $ionicModal, Data,GooglePlacesService){

//Check if user is already logged in
if(Data.checkAuthToken() != null){
	Data.checkAuthToken().then(function (results) {
			if(results.message){$state.go('app.feed');console.log('wel valid token');}
			else{console.log('invalid token');}

			});
		}

	$scope.bgs = ["img/welcome-bg.jpg"];
	$ionicModal.fromTemplateUrl('views/app/legal/privacy-policy.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.privacy_policy_modal = modal;
  });

	$ionicModal.fromTemplateUrl('views/app/legal/terms-of-service.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.terms_of_service_modal = modal;
  });

  $scope.showPrivacyPolicy = function() {

    GooglePlacesService.callAnalyticsEvent("Click","From legal page to privacy page","From legal page to privacy page");

    $scope.privacy_policy_modal.show();
  };

	$scope.showTerms = function() {

    GooglePlacesService.callAnalyticsEvent("Click","From legal page to terms page","From legal page to terms page");

    $scope.terms_of_service_modal.show();
  };
})

.controller('CreateAccountCtrl', function($scope, $state, $rootScope, Data, $ionicLoading){

	$scope.doSignUp = function(){
		//Store first name for later user
		localStorage.setItem('fname',$scope.user.fName);
//check if user is registering as a mechanic
var useriSMechanic = "";
if($scope.user.isMechanic){useriSMechanic = 1;} else{useriSMechanic = 0;}
 		var encodedCred =
								 'fname=' + encodeURIComponent($scope.user.fName) +
								 '&lname=' + encodeURIComponent($scope.user.lName) +
								 '&email=' + encodeURIComponent($scope.user.email) +
								 '&phone=' + encodeURIComponent($scope.user.phone) +
								 '&ismechanic=' + encodeURIComponent(useriSMechanic) +
								 '&password=' + encodeURIComponent($scope.user.password);

	Data.post('register',encodedCred).then(function (results) {
					console.log(results);
						if(results.error)
						{
							Data.toast("error","Ooopppsss!",results.message)

						}
						else {
								Data.toast("success","Registration Successful!",results.message)
							$state.go('welcome-back');
						}
			});
	};
})

.controller('WelcomeBackCtrl', function($scope, $state, $ionicModal, $http, Data){

	$scope.pwdreset = {
    user_new_password: '',
    user_password_reset_code: '',
    user_email: ''
  };

	$scope.fname = localStorage.getItem('fname');
	$ionicModal.fromTemplateUrl('views/auth/forgot-password.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.forgot_password_modal = modal;
  });

  $scope.showForgotPassword = function() {
    $scope.forgot_password_modal.show();
  };

	$scope.requestNewPassword = function(user_email) {
    console.log("requesting new password for: "+user_email);

		var encodedCred =
								 'email=' + encodeURIComponent(user_email);
								 console.log(encodedCred );
		Data.put('resetpasswordrequest',encodedCred).then(function(results)
	{
		console.log(results);
		if(!results.error)
		{
			$scope.passwordresetrequest = true;
			Data.toast("success","Password Recovery",results.message);
		}
		else {
			$scope.passwordresetrequest = false;
			Data.toast("error","Password Recovery",results.message);
		}

	})
  };

	$scope.resetPassword = function() {
		console.log(" user email: "+$scope.pwdreset.user_email+" user_new_password: "+$scope.pwdreset.user_new_password+" user_password_reset_code: "+$scope.pwdreset.user_password_reset_code);

		var encodedCred =
								 'email=' + encodeURIComponent($scope.pwdreset.user_email)+
								 '&new_password=' + encodeURIComponent($scope.pwdreset.user_new_password)+
								 '&reset_password_code=' + encodeURIComponent($scope.pwdreset.user_password_reset_code);
								 console.log(encodedCred );
	 Data.put('resetpassword',encodedCred).then(function(results)
	{
		console.log(results);
		if(!results.error)
		{
			$scope.passwordresetrequest = false;
			Data.toast("success","Password Recovery",results.message);
			$scope.forgot_password_modal.hide();
		}
		else {
			$scope.passwordresetrequest = true;
			Data.toast("error","Password Recovery",results.message);
		}

	})

	};

   //Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
   $scope.modal.hide();
  $scope.modal.remove();
  });
 // Execute action on hide modal
 $scope.$on('modal.hidden', function() {
  // Execute action
 });
// Execute action on remove modal
 $scope.$on('modal.removed', function() {
  // Execute action
   });
})

.controller('logOutCtrl', function ($scope, $state, $window, $ionicHistory, Data, $ionicLoading)
{
	//check user authtoken
	if(Data.checkAuthToken() != null){
		Data.checkAuthToken().then(function (results) {
				if(results.message){
				console.log('valid token');}
						else{console.log('invalid token');}

				});
			}else
			{
				$state.go('signin-account');
			}

	$scope.doLogOut = function ()
	{
		$window.localStorage.clear();
    $ionicHistory.clearCache();
    $ionicHistory.clearHistory();
		$state.go('signin-account');
		//$state.reload();
		$ionicLoading.hide(); //Fixed bug of showing loaind when logged out
		console.log("Logged out");
	}
})



.controller('authCtrl', function ($scope, $rootScope, $http, $state, Data, $ionicLoading) {
//check user authtoken
if(Data.checkAuthToken() != null){
		Data.checkAuthToken().then(function (results) {
				if(results.message){$state.go('app.feed');
				console.log('valid token');}
						else{console.log('invalid token');}

				});
			}






		$scope.doLogIn = function () {

			var encodedCred = 'email=' +
									 encodeURIComponent($scope.user.email) +
									 '&password=' +
									 encodeURIComponent($scope.user.password)+
									 '&device_id=' +
									 encodeURIComponent($rootScope.device_token);

		console.log('encodedCred',encodedCred);

		Data.post('login',encodedCred).then(function (results) {
            console.log(results);
							if(results.error)
							{
								Data.toast("error","Forgot your password?",results.message)
							}
							else {
								$scope.userId = localStorage.setItem('userId',results.id);
									$scope.userEmail = localStorage.setItem('userEmail',results.email);
									$scope.fname = localStorage.setItem('fname',results.fname);
									$scope.lname = localStorage.setItem('lname',results.lname);
									$scope.loggedUserIsMechanic = localStorage.setItem('ismechanic',results.ismechanic);
									$scope.userAuthToken = localStorage.setItem('userAuthToken',results.apiKey);
								//$state.go('app.feed');
								//$state.go('app.feed',null,{reload:true});
								Data.refresh();
								console.log("User logged in with userAuthToken:"+localStorage.getItem('userAuthToken'));
							}
        });
    };
})



.controller('ForgotPasswordCtrl', function($scope){

})

;
