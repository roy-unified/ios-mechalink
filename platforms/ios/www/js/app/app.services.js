angular.module('mechanlink.app.services', [])

.service('AuthService', function ($http, $q, Data, $ionicLoading, $state){

  this.getLoggedUser = function(){

    var dfd = $q.defer();
Data.get('userinfo').then(function(results)
{

  var user = results;
  dfd.resolve(user);
  console.log("Gotten loggedin user info");
})
    return dfd.promise;
  };

})

.service('NotificationService', function ($http, $q, Data, $ionicLoading){

    this.getUserNotifications = function(){
      var dfd = $q.defer();



      Data.get('notification/all').then(function(results)
    {

      console.log("user notification result");
      console.log(results);
      dfd.resolve(results);
    })

      return dfd.promise;
    };

    this.getUserNewNotificationsCount = function(){
      var dfd = $q.defer();



      Data.get('notification/new').then(function(results)
    {

      console.log("user new notification count");
      console.log(results);
      dfd.resolve(results);
    })

      return dfd.promise;
    };
})

.service('ProfileService', function ($http, $q, Data, $ionicLoading){

  this.getUserData = function(userId){

    var dfd = $q.defer();
Data.get('userinfo/'+userId).then(function(results){
  var user = results;
    console.log(userId);
  dfd.resolve(user);
})
    return dfd.promise;
  };

  this.getUserFollowers = function(userId){
    var dfd = $q.defer();

    Data.get('trustedby/'+userId).then(function(results)
  {
    console.log(results);

    dfd.resolve(results);
  });
  return dfd.promise;
  };

  this.getUserFollowing = function(userId){
    var dfd = $q.defer();



    Data.get('trust/'+userId).then(function(results)
  {
    //var dd = results.user_data.id;
    //var following = {
      //userId:dd,
      //userData: results.trusted_user_data,
    //};
    console.log("trust result");
    console.log(results);
    dfd.resolve(results);
  })

    return dfd.promise;
  };




  this.getUserPictures = function(userId){
    var dfd = $q.defer();

    $http.get('database.json').success(function(database) {
      //get user related pictures
      var user_pictures = _.filter(database.users_pictures, function(picture){
        return picture.userId == userId;
      });

      dfd.resolve(user_pictures);
    });

    return dfd.promise;
  };


  this.getUserServices = function(userId){
  var dfd = $q.defer();


  Data.get('allservices/'+userId).then(function(results){
    var user_services = results.services;
      console.log(user_services);

    dfd.resolve(user_services);
  })

      return dfd.promise;};


    this.getUserPosts = function(userId){
      var dfd = $q.defer();

      Data.get('alljobs/'+userId).then(function(results){
        var user_post = results.jobs;
          console.log(user_post);
        dfd.resolve(user_post);
      })

          return dfd.promise;};


  this.getIsTrusted = function(userId){
            var dfd = $q.defer();

            Data.get('istrusted/'+userId).then(function(results){
              var istrusted = results.istrusted;
                console.log(istrusted);
              dfd.resolve(istrusted);
            })

                return dfd.promise;};




})

.service('FeedService', function ($http, $q, Data, $ionicLoading){

  this.getFeed = function(page){

    var pageSize = 5, // set your page size, which is number of records per page
        skip = pageSize * (page-1),
        totalPosts = 1,
        totalPages = 1,
        dfd = $q.defer();

    /* $http.get('database.json').success(function(database) {

      totalPosts = database.posts.length;
      totalPages = totalPosts/pageSize;

      var sortedPosts =  _.sortBy(database.posts, function(post){ return new Date(post.date); });

      var postsToShow = sortedPosts.slice(skip, skip + pageSize);

      //add user data to posts
      var posts = _.each(postsToShow.reverse(), function(post){
        post.user = _.find(database.users, function(user){ return user.id == post.userId; });
        return post;
      });

      dfd.resolve({
        posts: posts,
        totalPages: totalPages
      });
    });
*/

    //test code
    var encodedCred =
                 'start=' + encodeURIComponent(skip) +
                 '&end=' + encodeURIComponent(skip + pageSize);

    Data.post('getjobfeed', encodedCred).then(function(results)
  {
          console.log("Data from feed service");
          console.log(results);
          totalPosts = results.totallivejobs;
          totalPages = totalPosts/pageSize;
          dfd.resolve({
              posts: results.jobs,
              totalPages: totalPages,
              totalPosts: totalPosts
            });
});

    return dfd.promise;
  };

  this.getUserActiveJobs = function(userId){
  var dfd = $q.defer();

  Data.get('job/active').then(function(results){
    var user_active_jobs = results;

    dfd.resolve(user_active_jobs);
  })

      return dfd.promise;};



  this.getFeedByCategory = function(page, categoryId){
console.log("Fetching by category");
    var pageSize = 5, // set your page size, which is number of records per page
        skip = pageSize * (page-1),
        totalPosts = 1,
        totalPages = 1,
        dfd = $q.defer();

    /*$http.get('database.json').success(function(database) {

      totalPosts = database.posts.length;
      totalPages = totalPosts/pageSize;

      var sortedPosts =  _.sortBy(database.posts, function(post){ return new Date(post.date); });

      if(categoryId){
        sortedPosts = _.filter(sortedPosts, function(post){ return post.category.id == categoryId; });
      }

      var postsToShow = sortedPosts.slice(skip, skip + pageSize);

      //add user data to posts
      var posts = _.each(postsToShow.reverse(), function(post){
        post.user = _.find(database.users, function(user){ return user.id == post.userId; });
        return post;
      });

      dfd.resolve({
        posts: posts,
        totalPages: totalPages
      });
    }); */

    var encodedCred =
                 'start=' + encodeURIComponent(skip) +
                 '&end=' + encodeURIComponent(skip + pageSize);

                 console.log("data to post for category fetching");
                 console.log(encodedCred +" "+categoryId);

    Data.post('getjobfeedbycategory/'+categoryId, encodedCred).then(function(results)
  {
    console.log("Fetching by category");

          console.log(results);

          totalPosts = results.totallivejobsincategory;
          totalPages = totalPosts/pageSize;
          dfd.resolve({
              posts: results.jobs,
              totalPages: totalPages
            });
});

    return dfd.promise;
  };


  this.getServiceFeedByCategory = function(page, categoryId){
console.log("Fetching service by category");
    var pageSize = 5, // set your page size, which is number of records per page
        skip = pageSize * (page-1),
        totalPosts = 1,
        totalPages = 1,
        dfd = $q.defer();

    /*$http.get('database.json').success(function(database) {

      totalPosts = database.posts.length;
      totalPages = totalPosts/pageSize;

      var sortedPosts =  _.sortBy(database.posts, function(post){ return new Date(post.date); });

      if(categoryId){
        sortedPosts = _.filter(sortedPosts, function(post){ return post.category.id == categoryId; });
      }

      var postsToShow = sortedPosts.slice(skip, skip + pageSize);

      //add user data to posts
      var posts = _.each(postsToShow.reverse(), function(post){
        post.user = _.find(database.users, function(user){ return user.id == post.userId; });
        return post;
      });

      dfd.resolve({
        posts: posts,
        totalPages: totalPages
      });
    }); */

    var encodedCred =
                 'start=' + encodeURIComponent(skip) +
                 '&end=' + encodeURIComponent(skip + pageSize);

                 console.log("data to post for category fetching");
                 console.log(encodedCred +" "+categoryId);

    Data.post('getservicefeedbycategory/'+categoryId, encodedCred).then(function(results)
  {
    console.log("Fetching service by category");

          console.log(results);

          totalPosts = results.totalservicesincategory;
          totalPages = totalPosts/pageSize;
          dfd.resolve({
              serviceposts: results.services,
              totalPages: totalPages
            });
});

    return dfd.promise;
  };





  this.getFeedByTrend = function(page, trendId){

    var pageSize = 5, // set your page size, which is number of records per page
        skip = pageSize * (page-1),
        totalPosts = 1,
        totalPages = 1,
        dfd = $q.defer();

    $http.get('database.json').success(function(database) {

      totalPosts = database.posts.length;
      totalPages = totalPosts/pageSize;

      var sortedPosts =  _.sortBy(database.posts, function(post){ return new Date(post.date); });

      if(trendId){
        sortedPosts = _.filter(sortedPosts, function(post){ return post.trend.id == trendId; });
      }

      var postsToShow = sortedPosts.slice(skip, skip + pageSize);

      //add user data to posts
      var posts = _.each(postsToShow.reverse(), function(post){
        post.user = _.find(database.users, function(user){ return user.id == post.userId; });
        return post;
      });

      dfd.resolve({
        posts: posts,
        totalPages: totalPages
      });
    });

    return dfd.promise;
  };

  this.getPostComments = function(post){
    var dfd = $q.defer();

    var commentid = post.id;
    //Show loading when fetching data from server

    Data.get('job/allcomment/'+commentid).then(function(results)
  {
    var comments_list = [];
    comments_list = results.comment;

    dfd.resolve(comments_list);
  })

    return dfd.promise;
  };




  this.getPostBidds = function(post){
    var dfd = $q.defer();

    Data.get('job/allbid/'+post.id).then(function(results)
  {
    var bids_list = [];
    bids_list = results;

    dfd.resolve(bids_list);
  })

    return dfd.promise;
  };


  this.getPostReviews = function(service){
    var dfd = $q.defer();

    var service_id = service.id;
    console.log(service.id);
    Data.get('service/allreview/'+service_id).then(function(results)
  {
    var reviews_list = [];
    reviews_list = results.review;
    console.log("Fetching reviews from feed service");
    console.log(results);
    dfd.resolve(reviews_list);
  })

    return dfd.promise;
  };


  this.getPost = function(postId){
    var dfd = $q.defer();

    Data.get('getjobpost/'+postId).then(function(results)
  {
    var post = results;

    dfd.resolve(post);
    console.log(post);
  });

    return dfd.promise;
  };

  this.getService = function(postId){
    var dfd = $q.defer();

    Data.get('getservicepost/'+postId).then(function(results)
  {
    var servicepost = results;

    dfd.resolve(servicepost);
    console.log(servicepost);
  });




    return dfd.promise;
  };





})

.service('MechanicsService', function ($http, $q, Data, $ionicLoading){

  this.getMechanicsSuggestions1 = function(){

    var dfd = $q.defer();

    $http.get('database.json').success(function(database) {

      var mechanics_suggestions = _.each(database.mechanics_suggestions, function(suggestion){
        suggestion.user = _.find(database.users, function(user){ return user.id == suggestion.userId; });

        //get user related pictures
        var user_pictures = _.filter(database.users_pictures, function(picture){
          return picture.userId == suggestion.userId;
        });

        suggestion.user.pictures = _.last(user_pictures, 3);

        return suggestion;
      });

      dfd.resolve(mechanics_suggestions);
    });

    return dfd.promise;
  };

  this.getMechanicsSuggestions = function(){
    var dfd = $q.defer();
    //$ionicLoading.show({template: "<icon class='icon ion-load-c'> </icon> <br/> Searching for mechanics in your area"});
    Data.get('mechanicsuggestion').then(function(results)
  {
    if(results == null){console.log("network error"); };
    var mechanics_suggestions = results;
$ionicLoading.hide();
    dfd.resolve(mechanics_suggestions);
  });
  return dfd.promise;
  };



  this.getSponsoredMechanics = function(){

    var dfd = $q.defer();

    $http.get('database.json').success(function(database) {

      var mechanics_you_may_know = _.each(database.mechanics_you_may_know, function(person){
        person.user = _.find(database.users, function(user){ return user.id == person.userId; });
        return person;
      });

      dfd.resolve(mechanics_you_may_know);
    });

    return dfd.promise;
  };
})


.service('TrendsService', function ($http, $q){
  this.getTrends = function(){
    var dfd = $q.defer();

    $http.get('database.json').success(function(database) {
      dfd.resolve(database.trends);
    });

    return dfd.promise;
  };

  this.getTrend = function(trendId){
    var dfd = $q.defer();

    $http.get('database.json').success(function(database) {
      var trend = _.find(database.trends, function(trend){ return trend.id == trendId; });
      dfd.resolve(trend);
    });

    return dfd.promise;
  };
})

.service('CategoryService', function ($http, $q){
  this.getCategories = function(){
    var dfd = $q.defer();

    $http.get('database.json').success(function(database) {
      dfd.resolve(database.categories);
    });

    return dfd.promise;
  };

  this.getCategory = function(categoryId){
    var dfd = $q.defer();

    $http.get('database.json').success(function(database) {
      var category = _.find(database.categories, function(category){ return category.id == categoryId; });
      dfd.resolve(category);
    });

    return dfd.promise;
  };
})

.service('GooglePlacesService', function($q){
  this.getPlacePredictions = function(query)
  {
    var dfd = $q.defer();
    var service = new google.maps.places.AutocompleteService();

    service.getPlacePredictions({ input: query },
      function(predictions, status){
        if (status != google.maps.places.PlacesServiceStatus.OK) {
          dfd.resolve([]);
        }
        else
        {
          dfd.resolve(predictions);
        }
      });
    return dfd.promise;
  }


    this.callAnalyticsEvent=function(category,action,label){

        document.addEventListener("deviceready", function(){

            console.log("action anlalysis");

            window.ga.trackEvent(category,action,label);


        }, false);

        return true;
    }
})


;
