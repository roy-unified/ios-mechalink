angular.module('underscore', ['toaster'])
    .factory('_', function () {
        return window._; // assumes underscore has already been loaded on the page
    });
angular.module('mechanlink', [
    'ionic',
    'mechanlink.common.directives',
    'mechanlink.app.services',
    'mechanlink.app.filters',
    'mechanlink.app.data',
    'mechanlink.app.controllers',
    'mechanlink.auth.controllers',
    'underscore',
    'angularMoment',
    'ngCordova',
    'monospaced.elastic',
    'toaster',
    'ksSwiper'

])

    .run(function ($ionicPlatform, $rootScope, $state,$location) {
        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);

            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }


            /*****************Whenever any state Changed happen *******************/

            $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
                $state.current = toState;
                console.log('Current state name .............................', $state.current);

                   // Prabhat Giri

                    document.addEventListener("deviceready", function(){

                        console.log(toState);

                        window.ga.trackView( toState.url, '', true,function(success){

                            console.log("state is changing");

                            console.log(success);
                        });

                    }, false);

            });


            /************************************************************************/


            /****************One signal Implementation ***************/


            var notificationOpenedCallback = function (jsonData) {

                console.log('notificationOpenedCallback: ', jsonData);

                var obj = {};
                obj = jsonData.notification.payload.additionalData;

                if (obj.notification_type == 1 || obj.notification_type == 2) {
                    var job_id = obj.reference_id;
                    $state.go('app.post', {postId: job_id});


                }

                if (obj.notification_type == 3) {

                    $state.go('app.servicepost', {postId: obj.reference_id});

                }

                if (obj.notification_type == 4 || obj.notification_type == 5 || obj.notification_type == 6) {

                    if (localStorage.getItem('ismechanic')==1) {

                        $state.go('app.profile.services', {userId: obj.trigger_user_id});

                    } else {
                        $state.go('app.profile.posts', {userId: obj.trigger_user_id});
                    }

                }

            };


            // TODO: Update with your OneSignal AppId before running.
            window.plugins.OneSignal.startInit("021ceee9-69d6-4866-9f2e-7e0d817d711a")
                .handleNotificationOpened(notificationOpenedCallback)
                .inFocusDisplaying(window.plugins.OneSignal.OSInFocusDisplayOption.None)
                .endInit();


            window.plugins.OneSignal.getIds(function (ids) {
                console.log("Full data Coming........................", ids);
                $rootScope.device_token = ids.userId;
                console.log("iD  for device" + $rootScope.device_token);
                // var notificationObj = {
                //     contents: {en: "message body"},
                //     include_player_ids: [ids.userId]
                // };
            });


            /**************One signal push notification*******************/


            /***************One signal Implementation *****************/


        });
    })

    .config(function ($stateProvider, $urlRouterProvider) {

        $stateProvider

        //SIDE MENU ROUTES
            .state('app', {
                url: "/app",
                abstract: true,
                templateUrl: "views/app/side-menu.html",
                resolve: {
                    loggedUser: function (AuthService) {
                        return AuthService.getLoggedUser();
                    },
                    feed: function (FeedService) {
                        // Default page is 1
                        var page = 1;

                        return FeedService.getFeed(page);
                    }
                }
                // controller: 'AppCtrl'
            })

            .state('app.feed', {
                url: "/feed",
                views: {
                    'menuContent': {
                        templateUrl: "views/app/feed.html",
                        controller: "FeedCtrl"
                    }
                },
                resolve: {
                    loggedUser: function (AuthService) {
                        return AuthService.getLoggedUser();
                    },
                    feed: function (FeedService) {
                        // Default page is 1
                        var page = 1;

                        return FeedService.getFeed(page);
                    },
                    notifications: function (NotificationService) {

                        return NotificationService.getUserNotifications();
                    },
                    newnotificationscount: function (NotificationService) {

                        return NotificationService.getUserNewNotificationsCount();
                    },
                    activeJobs: function (FeedService) {
                        return FeedService.getUserActiveJobs();
                    }
                }
            })

            .state('app.category_feed', {
                url: "/category_feed/:categoryId",
                views: {
                    'menuContent': {
                        templateUrl: "views/app/feed.html",
                        controller: "CategoryFeedCtrl"
                    }
                },
                resolve: {
                    loggedUser: function (AuthService) {
                        return AuthService.getLoggedUser();
                    },
                    feed: function (FeedService, $stateParams) {
                        // Default page is 1
                        var page = 1;
                        return FeedService.getFeedByCategory(page, $stateParams.categoryId);
                    },
                    notifications: function (NotificationService) {

                        return NotificationService.getUserNotifications();
                    },
                    newnotificationscount: function (NotificationService) {

                        return NotificationService.getUserNewNotificationsCount();
                    },
                    category: function (CategoryService, $stateParams) {
                        return CategoryService.getCategory($stateParams.categoryId);
                    }
                }
            })

            .state('app.trend_feed', {
                url: "/trend_feed/:trendId",
                views: {
                    'menuContent': {
                        templateUrl: "views/app/feed.html",
                        controller: "TrendFeedCtrl"
                    }
                },
                resolve: {
                    loggedUser: function (AuthService) {
                        return AuthService.getLoggedUser();
                    },
                    feed: function (FeedService, $stateParams) {
                        // Default page is 1
                        var page = 1;
                        return FeedService.getFeedByTrend(page, $stateParams.trendId);
                    },
                    trend: function (TrendsService, $stateParams) {
                        return TrendsService.getTrend($stateParams.trendId);
                    }
                }
            })


            .state('app.servicecategory_feed', {
                url: "/servicecategory_feed/:categoryId",
                views: {
                    'menuContent': {
                        templateUrl: "views/app/services.html",
                        controller: "ServiceCategoryFeedCtrl"
                    }
                },
                resolve: {
                    loggedUser: function (AuthService) {
                        return AuthService.getLoggedUser();
                    },
                    servicefeed: function (FeedService, $stateParams) {
                        // Default page is 1
                        var page = 1;
                        return FeedService.getServiceFeedByCategory(page, $stateParams.categoryId);
                    },
                    notifications: function (NotificationService) {

                        return NotificationService.getUserNotifications();
                    },
                    newnotificationscount: function (NotificationService) {

                        return NotificationService.getUserNewNotificationsCount();
                    },
                    category: function (CategoryService, $stateParams) {
                        return CategoryService.getCategory($stateParams.categoryId);
                    }
                }
            })


            .state('app.allservice_feed', {
                url: "/service_feed/:trendId",
                views: {
                    'menuContent': {
                        templateUrl: "views/app/services.html",
                        controller: "TrendFeedCtrl"
                    }
                },
                resolve: {
                    loggedUser: function (AuthService) {
                        return AuthService.getLoggedUser();
                    },
                    feed: function (FeedService, $stateParams) {
                        // Default page is 1
                        var page = 1;
                        return FeedService.getFeedByTrend(page, $stateParams.trendId);
                    },
                    trend: function (TrendsService, $stateParams) {
                        return TrendsService.getTrend($stateParams.trendId);
                    }
                }
            })


            .state('app.post', {
                url: "/post/:postId",
                views: {
                    'menuContent': {
                        templateUrl: "views/app/post/details.html",
                        controller: 'PostDetailsCtrl'
                    }
                },
                resolve: {
                    post: function (FeedService, $stateParams) {
                        return FeedService.getPost($stateParams.postId);
                    },

                }
            })

            .state('app.servicepost', {
                url: "/service/:postId",
                views: {
                    'menuContent': {
                        templateUrl: "views/app/post/servicedetails.html",
                        controller: 'ServiceDetailsCtrl'
                    }
                },
                resolve: {
                    service: function (FeedService, $stateParams) {
                        return FeedService.getService($stateParams.postId);
                    }
                }
            })

            .state('app.profile', {
                abstract: true,
                url: '/profile/:userId',
                views: {
                    'menuContent': {
                        templateUrl: "views/app/profile/profile.html",
                        controller: 'ProfileCtrl'
                    }
                },
                resolve: {
                    loggedUser: function (AuthService) {
                        return AuthService.getLoggedUser();
                    },
                    user: function (ProfileService, $stateParams) {
                        var profileUserId = $stateParams.userId;
                        return ProfileService.getUserData(profileUserId);
                    },
                    followers: function (ProfileService, $stateParams) {
                        var profileUserId = $stateParams.userId;
                        return ProfileService.getUserFollowers(profileUserId);
                    },
                    following: function (ProfileService, $stateParams) {
                        var profileUserId = $stateParams.userId;
                        return ProfileService.getUserFollowing(profileUserId);
                    },
                    posts: function (ProfileService, $stateParams) {
                        var profileUserId = $stateParams.userId;
                        return ProfileService.getUserPosts(profileUserId);
                    },
                    services: function (ProfileService, $stateParams) {
                        var profileUserId = $stateParams.userId;
                        return ProfileService.getUserServices(profileUserId);
                    },
                    pictures: function (ProfileService, $stateParams) {
                        var profileUserId = $stateParams.userId;
                        return ProfileService.getUserPictures(profileUserId);
                    },
                    notifications: function (NotificationService) {

                        return NotificationService.getUserNotifications();
                    },
                    newnotificationscount: function (NotificationService) {

                        return NotificationService.getUserNewNotificationsCount();
                    },
                    isTrusted: function (ProfileService, $stateParams) {
                        var profileUserId = $stateParams.userId;
                        return ProfileService.getIsTrusted(profileUserId);
                    }
                }
            })

            .state('app.profile.posts', {
                url: '/posts',
                views: {
                    'profileContent': {
                        templateUrl: 'views/app/profile/profile.details.html'
                    },
                    'profileSubContent@app.profile.posts': {
                        templateUrl: 'views/app/profile/profile.posts.html'
                    }
                }
            })

            .state('app.profile.pics', {
                url: '/pics',
                views: {
                    'profileContent': {
                        templateUrl: 'views/app/profile/profile.details.html'
                    },
                    'profileSubContent@app.profile.pics': {
                        templateUrl: 'views/app/profile/profile.pics.html'
                    }
                }
            })

            .state('app.profile.services', {
                url: '/servicesreviewed',
                views: {
                    'profileContent': {
                        templateUrl: 'views/app/profile/profile.details.html'
                    },
                    'profileSubContent@app.profile.services': {
                        templateUrl: 'views/app/profile/profile.services.html'
                    }
                }
            })

            .state('app.profile.followers', {
                url: "/followers",
                views: {
                    'profileContent': {
                        templateUrl: 'views/app/profile/profile.followers.html'
                    }
                }
            })

            .state('app.profile.notifications', {
                url: "/notifications",
                views: {
                    'profileContent': {
                        templateUrl: 'views/app/profile/profile.notifications.html',
                        controller: 'notificationCtrl'
                    }
                }
            })

            .state('app.profile.wonbids', {
                url: "/wonbids",
                views: {
                    'profileContent': {
                        templateUrl: 'views/app/profile/profile.wonbids.html'
                    }
                }
            })

            .state('app.profile.following', {
                url: "/following",
                views: {
                    'profileContent': {
                        templateUrl: 'views/app/profile/profile.following.html'
                    }
                }
            })

            .state('app.jobcategories', {
                url: "/jobcategories",
                views: {
                    'menuContent': {
                        templateUrl: "views/app/jobcategories.html",
                        controller: "BrowseCtrl"
                    }
                },
                resolve: {
                    trends: function (TrendsService) {
                        return TrendsService.getTrends();
                    },
                    categories: function (CategoryService) {
                        return CategoryService.getCategories();
                    },
                    loggedUser: function (AuthService) {
                        return AuthService.getLoggedUser();
                    }
                }
            })

            .state('app.mechanics', {
                url: "/mechanics",
                views: {
                    'menuContent': {
                        templateUrl: "views/app/mechanics.html",
                        controller: "MechanicsCtrl"
                    }
                },
                resolve: {
                    mechanics_suggestions: function (MechanicsService) {
                        return MechanicsService.getMechanicsSuggestions();
                    },
                    sponsored_mechanics: function (MechanicsService) {
                        return MechanicsService.getSponsoredMechanics();
                    }
                }
            })

            .state('app.services', {
                url: "/services",
                views: {
                    'menuContent': {
                        templateUrl: "views/app/services.html",
                        controller: "TrendFeedCtrl"
                    }
                },
                resolve: {
                    loggedUser: function (AuthService) {
                        return AuthService.getLoggedUser();
                    },
                    feed: function (FeedService, $stateParams) {
                        // Default page is 1
                        var page = 1;
                        return FeedService.getFeedByTrend(page, $stateParams.trendId);
                    },
                    trend: function (TrendsService, $stateParams) {
                        return TrendsService.getTrend($stateParams.trendId);
                    }
                }
            })

            .state('app.servicecategories', {
                url: "/servicecategories",
                views: {
                    'menuContent': {
                        templateUrl: "views/app/servicecategories.html",
                        controller: "BrowseCtrl"
                    }
                },
                resolve: {
                    trends: function (TrendsService) {
                        return TrendsService.getTrends();
                    },
                    categories: function (CategoryService) {
                        return CategoryService.getCategories();
                    },
                    loggedUser: function (AuthService) {
                        return AuthService.getLoggedUser();
                    }
                }
            })

            .state('app.settings', {
                url: "/settings",
                views: {
                    'menuContent': {
                        templateUrl: "views/app/profile/settings.html",
                        controller: 'SettingsCtrl'
                    }
                }
            })



            //AUTH ROUTES


            .state('signin-account', {
                url: "/signin-account",
                templateUrl: "views/auth/signin-account.html",
                controller: 'WelcomeCtrl'
            })

            .state('create-account', {
                url: "/create-account",
                templateUrl: "views/auth/create-account.html",
                controller: 'CreateAccountCtrl'
            })

            .state('welcome-back', {
                url: "/welcome-back",
                templateUrl: "views/auth/welcome-back.html",
                controller: 'WelcomeBackCtrl'
            });


        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/signin-account');





        document.addEventListener("deviceready", onDeviceReady, false);

        function onDeviceReady() {

            window.ga.startTrackerWithId('UA-83467878-7', 30,function(success){

                console.log("the response suc");

                console.log(success);

            },function(error){
                console.log("the response err");

                console.log(error);
            });





        }


    });


